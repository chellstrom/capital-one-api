package controllers

import models._
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.test.FakeRequest
import play.api.test.Helpers._

class TransactionControllerTest extends PlaySpec with GuiceOneAppPerSuite with ScalaFutures {

    private def accountRepo = app.injector.instanceOf(classOf[AccountRepository])
    private def transactionRepo = app.injector.instanceOf(classOf[TransactionRepository])
    private def transactionController = app.injector.instanceOf(classOf[TransactionController])

    private def fakeRequest(accountId: Int, `type`: String, amount: Int) = {
        FakeRequest().withBody(NewTransaction(accountId, `type`, amount))
    }

    "TransactionController" should {
        "add a purchase transaction" in {
            val newAccount = accountRepo.createAccount().futureValue

            val result = transactionController.createTransaction()(fakeRequest(newAccount.id, TransactionType.Purchase, 200))
            status(result) must equal(CREATED)

            val principalLedgerEntries = transactionRepo.getLedgerEntries(newAccount.id, LedgerType.Principal).futureValue
            principalLedgerEntries.length must equal(1)
            principalLedgerEntries.head.isCredit must be(0)

            val cashOutLedgerEntries = transactionRepo.getLedgerEntries(newAccount.id, LedgerType.CashOut).futureValue
            cashOutLedgerEntries.length must equal(1)
            cashOutLedgerEntries.head.isCredit must be(1)
        }

        "handle a non-existent account ID" in {
            val result = transactionController.createTransaction()(fakeRequest(-1, TransactionType.Purchase, 200))
            status(result) must equal(BAD_REQUEST)
        }

        "handle a bad purchase type" in {
            val newAccount = accountRepo.createAccount().futureValue

            val result = transactionController.createTransaction()(fakeRequest(newAccount.id, "badType", 200))
            status(result) must equal(BAD_REQUEST)
        }
    }
}
