package controllers

import models.{TransactionSummary, _}
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.libs.json.Json
import play.api.test.FakeRequest
import play.api.test.Helpers._

class AccountControllerTest extends PlaySpec with GuiceOneAppPerSuite with ScalaFutures {

    private def accountRepo = app.injector.instanceOf(classOf[AccountRepository])
    private def transactionRepo = app.injector.instanceOf(classOf[TransactionRepository])
    private def accountController = app.injector.instanceOf(classOf[AccountController])

    "AccountController" should {
        "get an existing account with no transactions" in {
            val newAccount = accountRepo.createAccount().futureValue

            val result = accountController.getAccount(newAccount.id)(FakeRequest())
            status(result) must equal(OK)

            val accountSummary = Json.fromJson[AccountSummary](contentAsJson(result)).get
            accountSummary must equal(AccountSummary(newAccount.id, 0, List()))
        }

        "get an existing account with a transaction history" in {
            val newAccount = accountRepo.createAccount().futureValue

            val transaction1 = transactionRepo.createTransaction(NewTransaction(newAccount.id, "purchase", 100), Some("2018-01-01T00:00:00Z")).futureValue
            val transaction2 = transactionRepo.createTransaction(NewTransaction(newAccount.id, "purchase", 200), Some("2018-01-02T00:00:00Z")).futureValue
            val transaction3 = transactionRepo.createTransaction(NewTransaction(newAccount.id, "purchase", 300), Some("2018-01-03T00:00:00Z")).futureValue

            val result = accountController.getAccount(newAccount.id)(FakeRequest())

            val accountSummary = Json.fromJson[AccountSummary](contentAsJson(result)).get
            accountSummary must equal(AccountSummary(newAccount.id, 600, List(
                TransactionSummary(transaction1.id, "purchase", "2018-01-01T00:00:00Z", 100),
                TransactionSummary(transaction2.id, "purchase", "2018-01-02T00:00:00Z", 200),
                TransactionSummary(transaction3.id, "purchase", "2018-01-03T00:00:00Z", 300)
            )))
        }

        "handle a non-existent account" in {
            val result = accountController.getAccount(-1)(FakeRequest())

            status(result) must equal(NOT_FOUND)
        }

        "create a new account" in {
            val result = accountController.createAccount()(FakeRequest())

            status(result) must equal(CREATED)

            val newAccount = Json.fromJson[Account](contentAsJson(result)).get
            newAccount.id must be > 0
        }
    }
}
