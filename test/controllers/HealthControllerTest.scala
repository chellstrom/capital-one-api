package controllers

import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.test.FakeRequest
import play.api.test.Helpers._

class HealthControllerTest extends PlaySpec with GuiceOneAppPerSuite with ScalaFutures {

    private def healthController = app.injector.instanceOf(classOf[HealthController])

    "HealthController" should {
        "return an OK status" in {
            val result = healthController.checkHealth()(FakeRequest())

            status(result) must equal(OK)
        }
    }
}
