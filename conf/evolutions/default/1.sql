# --- Capital One API DB

# --- !Ups

create table account (
    id integer primary key
);

create table ledger_type (
    id integer primary key,
    name text not null
);

insert into ledger_type(name) values ('principal');
insert into ledger_type(name) values ('cash-out');

create table transaction_type (
    id integer primary key,
    name text not null
);

insert into transaction_type(name) values ('purchase');

create table ledger (
    id integer primary key,
    type_id integer not null references ledger_type(id),
    transaction_id integer not null references journal(id),
    is_credit boolean not null
);

create table journal (
    id integer primary key,
    account_id integer not null references account(id),
    type_id integer not null references transaction_type(id),
    timestamp text not null default current_timestamp,
    amount integer not null
);

# --- !Downs

drop table account;
drop table ledger_type;
drop table transaction_type;
drop table ledger;
drop table journal;
