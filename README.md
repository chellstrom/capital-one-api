# Capital One Take Home Assignment

Author: Charles Hellstrom

This project contains an implementation of the simple credit card API take-home assignment.

I made the assumption that all transaction amounts are provided in cents, to avoid having to use floating point values.

## Setup

This project uses SBT to run.  To install SBT, download it [here](https://www.scala-sbt.org/download.html), or if you're using Mac OS, you can install it using Homebrew:

```
brew install sbt
```

## Running the server

Once you have SBT installed and the `sbt` command is in your path, you can start the API server by running

```
sbt run
```

in the root directory of this project.

## Making requests

After the server is started, you can make requests to the API at `localhost:9000`.

### Endpoints

#### GET /health

**Responses**

* `200` - if the server is up

#### GET /accounts/:id

**Response body**
```
{
    id: integer # unique ID of the account
    principal: integer # outstanding principal for the account, in cents
    transactions: [{ # list of transactions for this account, sorted by date ascending
        id: integer # unique ID of the transaction
        type: string # the type of transaction (currently only "purchase" is supported)
        timestamp: string # UTC timestamp of when the transaction happened
        amount: integer # cent amount associated with this transaction
    }]
}
```

**Responses**

* `200` - if the account with the given ID exists
* `404` - if the account does not exist

#### POST /accounts

**Response body**
```
{
    id: integer # unique ID of the newly created account
}
```

**Responses**

* `201` - if the account was created successfully

#### POST /transactions/:id

**Request body**
```
{
    accountId: integer # ID of the account to apply this transaction to
    type: string # the type of the transaction (currently only "purchase" is supported)
    amount: integer # the amount of the transaction in cents
}
```

**Responses**

* `201` - if the transaction was successfully created
* `400` - if the account does not exist or the type is invalid

## Running tests

To run the tests for this project, just run

```
sbt test
```

## Database

The server uses a simple SQLite database to store data.  The first time you hit an endpoint, the server will create a `capital-one-api.db` file, which will contain the database.

## Framework

This project uses the [Play application framework](https://playframework.com/), along with [Scala Anorm](https://www.playframework.com/documentation/2.6.x/ScalaAnorm) to talk to the database.

## TODOs

Some things I'd like to improve given more time on this assignment:

* Create a separate database that the tests can use
* Add end-to-end test that make actual HTTP calls against a running server
* Add Swagger docs for the endpoints
* Abstract out the logic about how transactions are split up into ledgers a bit better
* Use stronger typing for properties of the models