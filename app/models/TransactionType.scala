package models

/**
  * Correspond to the values in the `transaction_type` table.
  */
object TransactionType {
    val Purchase = "purchase"
}
