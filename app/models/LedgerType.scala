package models

/**
  * Correspond to the values in the `ledger_type` table.
  */
object LedgerType {
    val Principal = "principal"
    val CashOut = "cash-out"
}
