package models

import play.api.mvc.{Result, Results}

/**
  * Provides a convenience partial function to map exceptions to status codes
  */
object ExceptionMapper extends Results {
    val handleException: PartialFunction[Throwable, Result] = {
        case e: IllegalArgumentException => BadRequest(e.getMessage)
        case e: NoSuchElementException => NotFound(e.getMessage)
    }
}
