package models

import java.sql.Connection
import javax.inject.{Inject, Singleton}

import anorm._
import play.api.db.DBApi
import play.api.libs.json.{Format, Json}

import scala.concurrent.Future

case class Account(id: Int)
object Account {
    implicit val accountFormat: Format[Account] = Json.format[Account]
}

case class AccountSummary(id: Int, principal: Int, transactions: List[TransactionSummary])
object AccountSummary {
    implicit val accountFormat: Format[AccountSummary] = Json.format[AccountSummary]
}

case class TransactionSummary(id: Int, `type`: String, timestamp: String, amount: Int)
object TransactionSummary {
    implicit val transactionFormat: Format[TransactionSummary] = Json.format[TransactionSummary]
}

@Singleton
class AccountRepository @Inject()(dbapi: DBApi)(implicit ec: DatabaseExecutionContext) {
    private val db = dbapi.database("default")
    private val transactionSummaryParser: RowParser[TransactionSummary] = Macro.namedParser[TransactionSummary]
    private val accountParser: RowParser[Account] = Macro.namedParser[Account]

    /**
      * Creates a new account that will automatically be given the next available account ID.
      */
    def createAccount(): Future[Account] = Future(db.withConnection { implicit connection =>
        SQL"""
           insert into account default values
        """.execute()

        SQL"""
            select last_insert_rowid() as id
        """.as(accountParser.single)
    })

    /**
      * Returns the principal and transaction history for the given account.
      *
      * @throws NoSuchElementException if the account does not exists
      */
    def getAccountSummary(id: Int): Future[AccountSummary] = Future(db.withConnection { implicit connection =>
        assertAccountExists(id, missingOk = true)

        val principal = getAccountPrincipal(id)
        val transactions = getAccountTransactions(id)
        AccountSummary(id, principal, transactions)
    })

    /**
      * If an account with the given ID doesn't exist, throws an exception. If `missingOk` is `true`, throws a
      * `NoSuchElementException`.  Otherwise, throws an `IllegalArgumentException`.
      */
    def assertAccountExists(id: Int, missingOk: Boolean)(implicit connection: Connection): Unit = {
        val accountExists = SQL"""
            select exists(
                select 1
                from account
                where id = $id)
        """.as(SqlParser.int(1).single) == 1

        if (!accountExists) {
            if (missingOk) throw new NoSuchElementException(s"Account with ID $id could not be found")
            else throw new IllegalArgumentException(s"Account with ID $id does not exist")
        }
    }

    private def getAccountPrincipal(id: Int)(implicit connection: Connection): Int = {
        SQL"""
            select sum(j.amount) from ledger l
            inner join ledger_type lt on l.type_id = lt.id
            inner join journal j on l.transaction_id = j.id
            where lt.name = 'principal'
                  and l.is_credit = 0
                  and j.account_id = $id
        """.as(SqlParser.scalar[Int].singleOpt).getOrElse(0)
    }

    private def getAccountTransactions(id: Int)(implicit connection: Connection): List[TransactionSummary] = {
        SQL"""
            select j.id, j.timestamp, j.amount, tt.name as type from journal j
            inner join transaction_type tt on j.type_id = tt.id
            where account_id = $id
            order by j.id
        """.as(transactionSummaryParser.*)
    }
}
