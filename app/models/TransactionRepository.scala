package models

import java.sql.Connection
import javax.inject.{Inject, Singleton}

import anorm._
import org.joda.time.{DateTime, DateTimeZone}
import play.api.db.DBApi
import play.api.libs.json.{Format, Json}

import scala.concurrent.Future

case class NewTransaction(accountId: Int, `type`: String, amount: Int)
object NewTransaction {
    implicit val newTransactionFormat: Format[NewTransaction] = Json.format[NewTransaction]
}

case class Transaction(id: Int)
object Transaction {
    implicit val transactionFormat: Format[Transaction] = Json.format[Transaction]
}

case class LedgerEntry(id: Int, isCredit: Int)

@Singleton
class TransactionRepository @Inject()(dbapi: DBApi,
                                      accountRepo: AccountRepository)(implicit ec: DatabaseExecutionContext) {
    private val db = dbapi.database("default")
    private val ledgerEntryParser: RowParser[LedgerEntry] = Macro.namedParser[LedgerEntry]

    /**
      * Creates a new transaction and corresponding ledger entries.  If no `timestamp` value is provided, the current time is used.
      */
    def createTransaction(newTransaction: NewTransaction, timestamp: Option[String] = None): Future[Transaction] = Future(db.withTransaction { implicit connection =>
        accountRepo.assertAccountExists(newTransaction.accountId, missingOk = false)

        val typeId = getTransactionTypeId(newTransaction.`type`)

        val timestampParam = timestamp.getOrElse {
            new DateTime(DateTimeZone.UTC).toString()
        }
        SQL"""
           insert into journal(account_id, type_id, amount, timestamp) values(${newTransaction.accountId}, $typeId, ${newTransaction.amount}, $timestampParam)
        """.execute()

        val transactionId = SQL"""
            select last_insert_rowid()
        """.as(SqlParser.int(1).single)

        addLedgerEntries(transactionId, newTransaction.`type`)

        Transaction(transactionId)
    })

    /**
      * Returns all the ledger entries for the given account and ledger type.
      */
    def getLedgerEntries(accountId: Int, ledgerType: String): Future[List[LedgerEntry]] = Future(db.withTransaction { implicit connection =>
        accountRepo.assertAccountExists(accountId, missingOk = false)

        val ledgerTypeId = getLedgerTypeId(ledgerType)

        SQL"""
            select l.id, l.is_credit as isCredit from ledger l
            inner join journal j on l.transaction_id = j.id
            where j.account_id = $accountId
            and l.type_id = $ledgerTypeId
        """.as(ledgerEntryParser.*)
    })

    private def getTransactionTypeId(name: String)(implicit connection: Connection): Int = {
        SQL"""
            select tt.id from transaction_type tt
            where tt.name = $name
        """.as(SqlParser.int("id").singleOpt).getOrElse {
            throw new IllegalArgumentException(s"$name is not a valid transaction type")
        }
    }

    private def addLedgerEntries(transactionId: Int, transactionType: String)(implicit connection: Connection): Unit = {
        transactionType match {
            case TransactionType.Purchase => addPurchaseLedgerEntries(transactionId)
        }
    }

    private def getLedgerTypeId(name: String)(implicit connection: Connection): Int = {
        SQL"""
            select lt.id from ledger_type lt
            where lt.name = $name
        """.as(SqlParser.int("id").single)
    }

    private def addPurchaseLedgerEntries(transactionId: Int)(implicit connection: Connection): Unit = {
        val principalLedgerId = getLedgerTypeId(LedgerType.Principal)
        SQL"""
            insert into ledger(type_id, transaction_id, is_credit) values($principalLedgerId, $transactionId, 0)
        """.execute()

        val cashOutLedgerId = getLedgerTypeId(LedgerType.CashOut)
        SQL"""
            insert into ledger(type_id, transaction_id, is_credit) values($cashOutLedgerId, $transactionId, 1)
        """.execute()
    }
}
