package controllers

import javax.inject.{Inject, Singleton}

import models.AccountRepository
import models.ExceptionMapper.handleException
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.ExecutionContext


@Singleton
class AccountController @Inject()(cc: ControllerComponents,
                                  accountRepo: AccountRepository)
                                 (implicit ec: ExecutionContext) extends AbstractController(cc) {
    def createAccount(): Action[AnyContent] = Action.async { implicit request =>
        accountRepo.createAccount().map { newAccount =>
            Created(Json.toJson(newAccount))
        }
    }

    def getAccount(id: Int): Action[AnyContent] = Action.async { implicit request =>
        accountRepo.getAccountSummary(id).map { accountSummary =>
            Ok(Json.toJson(accountSummary))
        }.recover(handleException)
    }
}
