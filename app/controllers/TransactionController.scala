package controllers

import javax.inject.{Inject, Singleton}

import models.ExceptionMapper.handleException
import models._
import play.api.mvc.{AbstractController, Action, ControllerComponents}

import scala.concurrent.ExecutionContext

@Singleton
class TransactionController @Inject()(cc: ControllerComponents,
                                      accountRepo: AccountRepository,
                                      transactionRepo: TransactionRepository)
                                     (implicit ec: ExecutionContext) extends AbstractController(cc) {
    def createTransaction(): Action[NewTransaction] = Action.async(parse.json[NewTransaction]) { implicit request =>
        transactionRepo.createTransaction(request.body).map { _ =>
            Created
        }.recover(handleException)
    }
}
